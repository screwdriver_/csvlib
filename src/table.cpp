#include <csv.hpp>

#include <algorithm>
#include <fstream>
#include <iostream>

using namespace csv;

static inline bool is_line_break(const std::string &str, size_t i)
{
	return (i < str.size() && str[i] == '\n')
		|| (i + 1 < str.size() && str[i] == '\r' && str[i + 1] == '\n');
}

static size_t skip_line_breaks(const std::string &str, size_t begin)
{
	size_t i;

	i = begin;
	while(is_line_break(str, i))
	{
		if(str[i] == '\r')
			i += 2;
		else
			++i;
	}
	return i;
}

static std::string get_value(const std::string &str, size_t &i)
{
	std::string val;
	bool in_quote(false);

	while(i < str.size() && (in_quote || (str[i] != ',' && !is_line_break(str, i))))
	{
		if(str[i] == '"')
			in_quote = !in_quote;
		if(in_quote && i + 1 < str.size() && str[i] == '"' && str[i + 1] == '"')
		{
			val += '"';
			i += 2;
		}
		else
			val += str[i++];
	}
	return val;
}

table::table(const std::string &str)
{
	size_t i(0);

	while(i < str.size())
	{
		i = skip_line_breaks(str, i);
		if(i >= str.size())
			break;
		std::vector<value> values;
		while(!is_line_break(str, i))
			values.push_back(get_value(str, i));
		lines.emplace_back(values);
	}
}

table table::from_file(const std::string &file)
{
	std::ifstream stream(file);
	size_t size;
	std::string buff;

	// TODO Handle errors
	stream.seekg(0, std::ios::end);
	size = stream.tellg();
	stream.seekg(0, std::ios::beg);
	buff.resize(size);
	stream.read(&buff[0], size);
	return table(buff);

}

void table::insert_line(size_t i, const line &l)
{
	if(i >= lines.size())
		lines.push_back(l);
	else
		lines.insert(lines.begin() + i, l);
	if(lines.size() == 1)
		columns_count = l.columns_count();
	else if(lines[i].columns_count() < columns_count)
		lines[i].resize_columns(columns_count);
}

void table::remove_line(size_t i)
{
	if(i >= lines.size())
		return;
	lines.erase(lines.begin() + i);
}

void table::insert_column(size_t i)
{
	++columns_count;
	std::for_each(lines.begin(), lines.end(), [i](line &l)
		{
			l.insert_column(i);
		});
}

void table::remove_column(size_t i)
{
	--columns_count;
	std::for_each(lines.begin(), lines.end(), [i](line &l)
		{
			l.remove_column(i);
		});

}

std::string table::serialize() const
{
	std::string buff;
	size_t i;

	for(i = 0; i < lines.size(); ++i)
	{
		buff += lines[i].serialize();
		if(i + 1 < lines.size())
			buff += "\r\n";
	}
	return buff;
}
