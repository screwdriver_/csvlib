#ifndef CSV_HPP
# define CSV_HPP

# include <string>
# include <vector>
namespace csv
{
	using value = std::string;

	class table;

	class line
	{
		friend class table;

		public:
		line() = default;
		line(const std::vector<std::string> &values);

		inline value &get_value(size_t i)
		{
			return values[i];
		}

		inline const value &get_value(size_t i) const
		{
			return values[i];
		}

		inline value &operator[](size_t i)
		{
			return get_value(i);
		}

		inline const value &operator[](size_t i) const
		{
			return get_value(i);
		}

		inline size_t columns_count() const
		{
			return values.size();
		}

		// TODO Iterators?

		std::string serialize() const;

		protected:
		void resize_columns(size_t count);
		void insert_column(size_t i);
		void remove_column(size_t i);

		private:
		std::vector<value> values;
	};

	class table
	{
		public:
		table(const std::string &str);

		static table from_file(const std::string &file);

		void insert_line(size_t i, const line &l);
		void remove_line(size_t i);

		inline line &get_line(size_t i)
		{
			return lines[i];
		}

		inline const line &get_line(size_t i) const
		{
			return lines[i];
		}

		inline table operator+(const line &l) const
		{
			table t(*this);
			t += l;
			return t;
		}

		inline void operator+=(const line &l)
		{
			append_line(l);
		}

		inline line &operator[](size_t i)
		{
			return get_line(i);
		}

		inline const line &operator[](size_t i) const
		{
			return get_line(i);
		}

		inline value &operator()(size_t line, size_t col)
		{
			return get_line(line)[col];
		}

		inline const value &operator()(size_t line, size_t col) const
		{
			return get_line(line)[col];
		}

		inline void append_line(const line &l)
		{
			insert_line(line_count(), l);
		}

		void insert_column(size_t i);
		void remove_column(size_t i);

		inline size_t line_count() const
		{
			return lines.size();
		}

		inline size_t column_count() const
		{
			return columns_count;
		}

		// TODO Iterators?

		std::string serialize() const;

		private:
		std::vector<line> lines;
		size_t columns_count;
	};
}

#endif
