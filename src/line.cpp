#include <csv.hpp>

using namespace csv;

line::line(const std::vector<std::string> &values)
	: values{values}
{}

void line::resize_columns(size_t count)
{
	values.resize(count);
}

void line::insert_column(size_t i)
{
	values.insert(values.begin() + i, "");
}

void line::remove_column(size_t i)
{
	values.erase(values.begin() + i);
}

static std::string serialize_value(const value &val)
{
	std::string str;
	size_t i;

	str += '"';
	for(i = 0; i < val.size(); ++i)
	{
		if(val[i] == '"')
			str += "\"\"";
		else
			str += val[i];
	}
	str += '"';
	return str;
}

std::string line::serialize() const
{
	std::string str;
	size_t i;

	for(i = 0; i < values.size(); ++i)
	{
		str += serialize_value(values[i]);
		if(i + 1 < values.size())
			str += ',';
	}
	return str;
}
